## Implement sample CleanArchitecture With Asp.NET Core 5

# Requirements : .Net 5 , Postgresql

- please clone this Repository with

```
$   git clone git@gitlab.com:abolfalahangar/cleanarchitecture_with_asp.netcore.git
```

## Running the app

```bash
# development
$ dotnet watch run

```
